import vue from 'vue'
import Multiselect from 'vue-multiselect'
import CoolLightBox from 'vue-cool-lightbox'
import Paginate from 'vuejs-paginate'

vue.component('paginate', Paginate)
vue.component('multiselect', Multiselect)
vue.use(CoolLightBox)
